todo リストの作成を行って頂きます。

Bitbucket（

[https://bitbucket.org/](https://bitbucket.org/)

）に課題用 git リポジトリを作成し、以下の仕様を満たすプログラムを作成してください。

【仕様】

1. Todo の登録と編集、削除ができること。 ok

2. Todo の完了ができること。 ok

3. Todo を「今日の予定」や「あとで見る」など種類ごとに整理できること。 ok

4. ブラウザを再起動しても Todo が消えないようにする。(シークレットモードを除く) ok

5. 下記の動作環境で動作するように実装してください。

[PC]

Microsoft Edge 　 ok

Firefox の最新版 ok

Google Chrome の最新版 ok

[iOS]

iOS の最新版の safari

[Android]

Android の最新版の Google Chrome

上記の他に、自由にユーザーが使いやすいと思う機能追加をしてください。

(例：登録や編集日時の記録やリマインダー、キーボード操作など)

【補足・注意事項】

- TODO リストに関するコード(HTML / CSS / Javascript)を実装し、提出してください。
- Javascript のコーディングに際して、Babel や webpack など開発環境を構築するような外部ライブラリを使用して頂いて構いません。
- React や jQuery など開発環境以外の外部ライブラリは使用しないでください。
- css 作成に際して、sass などを使用して頂いて構いません。
- 使いやすい UI/UX を考慮した実装をしてください。

【提出に関して】

提出期限： 3 月 7 日（月） （ドイツ時間で日付が替わるまで）

提出先：

[recruit@papyrusde.com](mailto:recruit@papyrusde.com)

Bitbucket の共有（共有先 →

[recruit@papyrusde.com](mailto:recruit@papyrusde.com)

）をお願いします。

※ 共有いただいたリポジトリの master ブランチのファイルを提出物として判断致します。

※ 作業時のコミット履歴は削除しないでください。

※ 提出いただいたソースファイルに不足がある場合は、不足分を考慮せずに判断致します。

**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

_We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket._

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: _Delete this line to make a change to the README from Bitbucket._
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
